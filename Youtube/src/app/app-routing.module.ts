import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component'
import {RegisterComponent} from './register/register.component';
import {PagenotfoundComponent} from './pagenotfound/pagenotfound.component';
import {CalculatorComponent} from './calculator/calculator.component';
import {VideosComponent} from './videos/videos.component';
import {ChannelsComponent} from './channels/channels.component';

const routes: Routes = [
  /*{path : "", component: RegisterComponent},*/
  {path:"channels",component : ChannelsComponent},
  {path:"", redirectTo:'/register',pathMatch: 'full'},
  {path : "login", component: LoginComponent},
  {path : "register", component: RegisterComponent},
  {path : "calculator", component: CalculatorComponent},
  {path:"channels/:channelid", component : VideosComponent},
  {path:"**", component: PagenotfoundComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingcomponents=[LoginComponent,RegisterComponent,PagenotfoundComponent,VideosComponent,ChannelsComponent]


