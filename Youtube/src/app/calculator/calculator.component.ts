import { Component, OnInit } from '@angular/core';
import {CalculatorService} from './calculator.service'
import {Calc} from'./calc';
@Component({
  selector: 'app-calculator',
  template: `

  <div class="container-fluid" style='text-align:center'>
  <h1 style="text-align: center;
  text-transform: uppercase;
  color: #4CAF50;">Calculator</h1>

  <form #calcform="ngForm"    novalidate>
        <div class="form-group">
          <label class="text">First Number</label><br>     
          <input type="number" name="num1" required   #num1="ngModel" [class.backcss]="num1.invalid && num1.touched" class="form-control" [(ngModel)]="calcmodel.num1">
        </div>                
        <small class="text-danger" *ngIf="num1.invalid && num1.touched">number is Required</small>
        <div class="form-group">
          <label class="text">Second Number</label><br>     
          <input type="number" name="num2" required   #num2="ngModel" [class.backcss]="num2.invalid && num2.touched" class="form-control" [(ngModel)]="calcmodel.num2">
        </div>                
        <small class="text-danger" *ngIf="num2.invalid && num2.touched">number is Required</small>
       <br><br>
        <div class="form-group">
          <button type="submit" (click)="onsub()" [disabled]="calcform.form.invalid"  class="submitbutton">Substract</button>
       </div>
  </form>
 <h3 style="text-align: center;       
 color: #4CAF50"> {{calcdata}}</h3>
</div>
  
 
  `,
  styles: [
`
.backcss
{
  background-color:rgb(178,34,34);
}


.submitbutton{
  background-color: #4CAF50; 
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}

label.text {
  white-space: nowrap; 
  width: 900px; 
  color:green;
}
.text-danger
{
color:red;
}
input[type=text]{
  width: 50%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container-fluid {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}


` ]
})
export class CalculatorComponent implements OnInit {

  calcmodel=new  Calc(7,7)
  constructor(private calculatorservice:CalculatorService) { }

  ngOnInit() {
  }

  calcdata;

  onsub()
  {
      this.calculatorservice.sub(this.calcmodel)
      .subscribe(data=>{this.calcdata=JSON.stringify(data)},
      error=>{console.error(error.message);}
      )
  }
}
