import { Injectable } from '@angular/core';

import { HttpClient, HttpResponse ,HttpHeaders} from '@angular/common/http';
import {Calc} from './calc'
@Injectable({
  providedIn: 'root'
})
export class CalculatorService {
  calcmodel=new  Calc(6,7)
  public sub_url="http://127.0.0.1:8000/calculate/numbers"
  constructor(private http:HttpClient) { }
  

  
  sub(calc:Calc)
  {
    return this.http.put<any>(this.sub_url,calc)
  }
}
