import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse ,HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
@Component({
  selector: 'app-channels',
  template: `
    <div class="grid">
      <div>
      <img (click)="gotovideos('UC1NF71EwP41VdjAU1iXdLkw')" src="assets/img/narendramodi.jpg">
      <a (click)="gotovideos('UC1NF71EwP41VdjAU1iXdLkw')"><h3>Narendra Modi</h3></a>
      </div>
      <div>
      <img (click)="gotovideos('UCSiDGb0MnHFGjs4E2WKvShw')"   src="assets/img/mrindianhacker.jpg">
      <a (click)="gotovideos('UCSiDGb0MnHFGjs4E2WKvShw')"><h3>Mr Indian Hacker</h3></a>
      </div>
      <div>
      <img  (click)="gotovideos('UCt-am4x4ZyeohfgM85qFgdQ')" src="assets/img/elliegoudling.jpg">
      <a (click)="gotovideos('UCt-am4x4ZyeohfgM85qFgdQ')"><h3>Ellie Goulding</h3></a>
      </div>
      <div>
      <img  (click)="gotovideos('UCfgZg9Vl5AwedH14BYgLXTQ')" src="assets/img/sidtalk.png">
      <a (click)="gotovideos('UCfgZg9Vl5AwedH14BYgLXTQ')"><h3>Sid Talk</h3></a>
      </div>
    </div>

  `,
  styles: [
`
.grid
{
display:grid;
grid-template-columns: auto auto auto auto;
grid-gap: 10px;
background-color: #FF3457;
padding: 10px;
border:1px solid brown;
}
`  ]
})
export class ChannelsComponent implements OnInit {


  constructor(private route:Router) { }

  ngOnInit() 
  {

  }

  gotovideos(channelid:string)
  {
    this.route.navigate(['/channels',channelid])
  }

}
