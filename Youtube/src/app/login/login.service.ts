import { Injectable } from '@angular/core'
import { HttpClient, HttpResponse ,HttpHeaders} from '@angular/common/http';;

import {Loginuser} from './loginuser';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public log_url="http://127.0.0.1:8000/calculate/login";
  constructor(private http:HttpClient) { }

  login(loginuser:Loginuser)
  {
        return this.http.post<any>(this.log_url,loginuser)
  }




}
