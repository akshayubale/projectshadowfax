import { Component, OnInit } from '@angular/core';
import {LoginService} from './login.service'
import{ Observable} from 'rxjs';

import {Loginuser} from './loginuser'

import { HttpClient, HttpResponse ,HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  template: `
  <div class="container-fluid" style="text-align:center">
  <h2 style="text-align: center;
  text-transform: uppercase;
  color: rgb(1, 17, 2);">
  
    {{responseuserdata}}
  </h2>
 
    <div class="container-fluid" style='text-align:center'>
        <h1 style="text-align: center;
        text-transform: uppercase;
        color: #4CAF50;">Login Here</h1>
        <br>
        <form #loginuserform="ngForm"  (ngSubmit)="onlogin()"  novalidate>
              <div class="form-group">
                <label class="text">Username</label><br>     
                <input type="text" name="username" required   #username="ngModel" [class.backcss]="username.invalid && username.touched" class="form-control" [(ngModel)]="loginusermodel.username">
              </div>                
              <small class="text-danger" *ngIf="username.invalid && username.touched">Username is Required</small>
                
                <div class="form-group">
                   <label class="text">Password</label><br>
                  <input type="text" name="password" required   #password="ngModel" [class.backcss]="password.invalid && password.touched"
                  class="form-control"[(ngModel)]="loginusermodel.password"> 
                </div>
                <small class="text-danger" *ngIf="password.invalid && password.touched">Password is Required</small><br>                
                
               <div class="form-group">
                  <button type="submit"  [disabled]="loginuserform.form.invalid"  class="submitbutton">Login</button>
               </div>
               <br><br>
            </form>
            <!--
            <h3 style="text-align: center;
            color: #4CAF50;"> Not a User?</h3>
               <div class="form-group">
                <button type="submit"  (click)="onregister()" class="submitbutton">Register</button>
             </div>  
             -->     
    </div>
  
  </div>
  `,
  styles: [
    `
    .backcss
    {
      background-color:rgb(178,34,34);
    }
    
    
    .submitbutton{
      background-color: #4CAF50; 
      border: none;
      color: white;
      padding: 15px 32px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
    }
    
    label.text {
      white-space: nowrap; 
      width: 900px; 
      color:green;
    }
    .text-danger
    {
    color:red;
    }
    input[type=text]{
      width: 25%;
      padding: 8px 8px;
      margin: 8px 0;
      display: inline-block;
      border: 1px solid #ccc;
      border-radius: 4px;
      box-sizing: border-box;
    }
    
    input[type=submit]:hover {
      background-color: #45a049;
    }
    
    .container-fluid {
      border-radius: 5px;
      background-color: #f2f2f2;
      padding: 20px;
    }   
    
    ` ]
})
export class LoginComponent implements OnInit {

  loginusermodel=new Loginuser("","")
  constructor(private loginservice : LoginService,private router:Router){}

  onregister()
  {
    this.router.navigate((['/register']))
  }

  token:string;  
  responseloginusererrormsg="";
  
  
  onlogin()
  {                             
        this.loginservice.login(this.loginusermodel)
        .subscribe(data=>{this.token=JSON.parse(JSON.stringify(data)).token;this.router.navigate(['/calculator'])},
          error=>{this.responseloginusererrormsg=error.message;this.router.navigate(['/login'])}         
          )          
  } 

  ngOnInit() {
  }

}
