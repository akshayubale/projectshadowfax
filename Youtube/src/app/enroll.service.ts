import { Injectable } from '@angular/core';

import { HttpClient, HttpResponse ,HttpHeaders} from '@angular/common/http';
import { User } from './register/user';
import {Loginuser} from './login/loginuser';
import{Calc} from './calculator/calc'

@Injectable({
  providedIn: 'root'
})
export class EnrollService {
  public reg_url="http://127.0.0.1:8000/calculate/register";
  public log_url="http://127.0.0.1:8000/calculate/login";
  public sub_url="http://127.0.0.1:8000/calculate/numbers"
   constructor(private http:HttpClient) { }
 
   enroll(user:User)
   {
     return this.http.post<any>(this.reg_url,user)
   }
 
   login(loginuser:Loginuser)
   {
         return this.http.post<any>(this.log_url,loginuser)
   }
 
 
  
   sub(calc:Calc)
   {
     return this.http.put<any>(this.sub_url,calc)
   }
 
}
