import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse ,HttpHeaders} from '@angular/common/http';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  public reg_url="http://127.0.0.1:8000/calculate/register";
  constructor(private http:HttpClient) { }
 
   enroll(user:User)
   {
     return this.http.post<any>(this.reg_url,user)
   }
}
