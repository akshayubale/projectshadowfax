import { Component, OnInit } from '@angular/core';
import {RegisterService} from './register.service';
import{ Observable} from 'rxjs';
import {User} from './user'

import { HttpClient, HttpResponse ,HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  template: `
  <div class="container-fluid" style="text-align: center" >
  <h1 style="text-align: center;
  text-transform: uppercase;
  color: #4CAF50; ">Register Here</h1>
  <br>
  
  <form #userform="ngForm" (ngSubmit)="onsubmit()" novalidate>
  
  <!--
  {{userform.value | json}}
  <hr />
  {{usermodel | json}}
  -->
  
  <div class="form-group" >
  <label class="text">Firstname</label><br>
  <input type="text"  name="first_name" required  #first_name="ngModel" [class.backcss]="first_name.invalid && first_name.touched"  class="form-control" [(ngModel)]="usermodel.first_name">
  </div>
  
  
  <small class="text-danger" *ngIf="first_name.invalid && first_name.touched">First  Name is Required</small>
  
  <!--{{name.className}}-->
  <!--{{name.invalid}}-->
  
  
  <div class="form-group">
  <label class="text">Lastname</label><br>
  <input type="text" name="last_name" required  #last_name="ngModel" [class.backcss]="last_name.invalid && last_name.touched" class="form-control" [(ngModel)]="usermodel.last_name">
  </div>
  
  <small class="text-danger" *ngIf="last_name.invalid && last_name.touched">Last Name is Required</small>
  
  <div class="form-group">
  <label class="text">Email    </label><br>
  <input type="text" name="email" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required #email="ngModel"  [class.backcss]="email.invalid && email.touched" class="form-control" [(ngModel)]="usermodel.email">    
  </div>
  
  <!--<small class="text-danger" *ngIf="email.untouched">Email is Required</small> -->
  <div  *ngIf="email.errors && (email.invalid && email.touched)">
  <small class="text-danger" *ngIf=" email.errors.pattern" > Provide correct Email ID</small>
  <small class="text-danger" *ngIf=" email.errors.required" > Email ID is required</small>
  </div>
  <div class="form-group">
  <label class="text">Username</label><br>     
  <input type="text" name="username" required   #username="ngModel" [class.backcss]="username.invalid && username.touched" class="form-control" [(ngModel)]="usermodel.username">
  </div>
  
  <small class="text-danger" *ngIf="username.invalid && username.touched">Username is Required</small>
  
  <div class="form-group">
  <label class="text">Password</label><br>
  <input type="text" name="password" required   #password="ngModel" [class.backcss]="password.invalid && password.touched"
  class="form-control"[(ngModel)]="usermodel.password"> 
  </div>
  <small class="text-danger" *ngIf="password.invalid && password.touched">Password is Required</small><br>
  
  
  
  <div class="form-group">
  <button type="submit"  [disabled]="userform.form.invalid"  class="submitbutton">Register</button>
  </div>
  </form>
  <br><br>
  <!--
  <div class="form-group">
  <button type="submit"   (click)="directlogin()" class="submitbutton">Already a User?</button>
  </div>
  -->
  
  </div>
  
  `,
  styles: [
    `
    .backcss
    {
      background-color:rgb(178,34,34);
    }
    
    
    .submitbutton{
      background-color: #4CAF50; 
      border: none;
      color: white;
      padding: 15px 32px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
    }
    
    label.text {
      white-space: nowrap; 
      width: 900px; 
      color:green;
    }
    .text-danger
    {
    color:red;
    }
    input[type=text]{
      width: 25%;
      margin: 8px 0;
      padding: 8px 8px;
      display: inline-block;
      border: 1px solid #ccc;
      border-radius: 4px;
      box-sizing: border-box;
    }
    
    input[type=submit]:hover {
      background-color: #45a049;
    }
    
    .container-fluid {
      border-radius: 5px;
      background-color: #f2f2f2;
      padding: 20px;
    }
    ` ]
})
export class RegisterComponent implements OnInit {
  usermodel=new User("","","","","")
  constructor(private registerservice : RegisterService, private router :Router){}

  ngOnInit() {
  }
  public responseuserdata="";
  responseusererrormsg: string;
  

  onsubmit()
  {   
    this.registerservice.enroll(this.usermodel)
    .subscribe(data=>{this.responseuserdata=data;this.router.navigate(['/login'])},
     error=>{this.responseusererrormsg=error.message;this.router.navigate(['/register'])})     
  }
  
  directlogin()
  {
    this.router.navigate(['/login'])
  }

}
