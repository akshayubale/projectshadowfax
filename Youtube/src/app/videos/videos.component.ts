import { Component, OnInit } from '@angular/core';
import {VideosService} from './videos.service'
import {ActivatedRoute,Router,ParamMap} from '@angular/router'
import { ReplaySubject } from 'rxjs'

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styles: [
`
.button
{
  background-color: #4CAF99;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
.grid
{
display:grid;
grid-template-columns: auto auto auto auto;
grid-gap: 10px;
background-color: #FF3457;
padding: 10px;
border:1px solid brown;
}
.grid > div
{
background-color: lightgreen;
padding: 10px;
border:1px solid black;
}
`]
})
export class VideosComponent implements OnInit {
  videos:any=[];
  channelid:string =""
   
  ngOnInit() 
  {
    
  }
  limit=50;
  p:number=1;
  constructor(private videosservice:VideosService,private route: ActivatedRoute,private router:Router) 
  {
    this.route.paramMap.subscribe((params:ParamMap)=>
    {
    let id=params.get('channelid');
    this.channelid=id;
    console.info(this.channelid)
    }
    )
         
    this.videosservice.getvideosforchannel(this.channelid,this.limit)
    .subscribe(lista=>
       {
        for(let element of lista["items"])
        {
          this.videos.push(element)
        }        
      }
      )  
      
  }

  goback()
  {
    this.router.navigate(['/channels'])
  }
  
  player:YT.Player
  savePlayer(player)
  {
    this.player=player;
  }
 
}
