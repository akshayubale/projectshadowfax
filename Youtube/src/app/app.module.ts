import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule,routingcomponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { CalculatorComponent } from './calculator/calculator.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {LoginService} from './login/login.service';
import {VideosService} from'./videos/videos.service';
import {RegisterService} from './register/register.service';
import {CalculatorService} from './calculator/calculator.service'
import {HttpClientModule} from '@angular/common/http';
import { VideosComponent } from './videos/videos.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { ChannelsComponent } from './channels/channels.component';
import { YoutubePlayerModule } from 'ng2-youtube-player';


@NgModule({
  declarations: [
    AppComponent,
    routingcomponents,
    PagenotfoundComponent,
    CalculatorComponent,
    LoginComponent,
    RegisterComponent,
    VideosComponent,
    ChannelsComponent,
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    YoutubePlayerModule,
    
  ],
  providers: [LoginService,CalculatorService,RegisterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
